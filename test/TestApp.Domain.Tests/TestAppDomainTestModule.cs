﻿using TestApp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace TestApp
{
    [DependsOn(
        typeof(TestAppEntityFrameworkCoreTestModule)
        )]
    public class TestAppDomainTestModule : AbpModule
    {

    }
}