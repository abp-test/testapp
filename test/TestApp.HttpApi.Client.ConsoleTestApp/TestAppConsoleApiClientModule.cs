﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace TestApp.HttpApi.Client.ConsoleTestApp
{
    [DependsOn(
        typeof(TestAppHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class TestAppConsoleApiClientModule : AbpModule
    {
        
    }
}
