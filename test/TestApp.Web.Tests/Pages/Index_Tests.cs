﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace TestApp.Pages
{
    public class Index_Tests : TestAppWebTestBase
    {
        [Fact]
        public async Task Welcome_Page()
        {
            var response = await GetResponseAsStringAsync("/");
            response.ShouldNotBeNull();
        }
    }
}
