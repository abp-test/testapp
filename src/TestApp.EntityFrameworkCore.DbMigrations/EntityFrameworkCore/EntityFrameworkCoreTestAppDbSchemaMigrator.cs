﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestApp.Data;
using Volo.Abp.DependencyInjection;

namespace TestApp.EntityFrameworkCore
{
    [Dependency(ReplaceServices = true)]
    public class EntityFrameworkCoreTestAppDbSchemaMigrator 
        : ITestAppDbSchemaMigrator, ITransientDependency
    {
        private readonly TestAppMigrationsDbContext _dbContext;

        public EntityFrameworkCoreTestAppDbSchemaMigrator(TestAppMigrationsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task MigrateAsync()
        {
            await _dbContext.Database.MigrateAsync();
        }
    }
}