﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestApp.EntityFrameworkCore;
using TestApp.Models.Test;

namespace TestApp.Controllers
{
    [Route("api/test")]
    public class TestController : TestAppController
    {
        private readonly TestAppDbContext _dbContext;

        // Injected only for testing and demonstration of db calls
        public TestController(TestAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        [Route("")]
        public async Task<List<TestModel>> GetAsync()
        {
            // Simulate some db load
            for (var i = 0; i < 100; i++)
            {
                await _dbContext.Users.ToListAsync();
            }
            
            return await _dbContext.Users.AsNoTracking().Select(x => new TestModel
            {
                Name = x.UserName,
                BirthDate = x.CreationTime
            }).ToListAsync();

            /*return new List<TestModel>
            {
                new TestModel {Name = "John", BirthDate = new DateTime(1942, 11, 18)},
                new TestModel {Name = "Adams", BirthDate = new DateTime(1997, 05, 24)}
            };*/
        }
    }
}