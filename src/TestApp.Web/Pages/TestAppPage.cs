﻿using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using TestApp.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace TestApp.Web.Pages
{
    /* Inherit your UI Pages from this class. To do that, add this line to your Pages (.cshtml files under the Page folder):
     * @inherits TestApp.Web.Pages.TestAppPage
     */
    public abstract class TestAppPage : AbpPage
    {
        [RazorInject]
        public IHtmlLocalizer<TestAppResource> L { get; set; }
    }
}
