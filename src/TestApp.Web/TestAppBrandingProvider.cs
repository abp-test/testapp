﻿using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Components;
using Volo.Abp.DependencyInjection;

namespace TestApp.Web
{
    [Dependency(ReplaceServices = true)]
    public class TestAppBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "TestApp";
    }
}
